import { MODES } from '../../app/modes';
import { SET } from '../../app/reasons';

function getTransactionMode(transaction) {
  if (transaction.reason === SET) {
    return MODES.SET;
  }
  return transaction.amount > 0 ? MODES.POS : MODES.NEG;
}

export default getTransactionMode;
