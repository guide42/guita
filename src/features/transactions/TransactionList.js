import React, { useState, useCallback, useEffect, useContext, useRef } from 'react';
import { useSelector } from 'react-redux';
import { createSelector } from '@reduxjs/toolkit';
import { MODES } from '../../app/modes';
import { getReasonName } from '../../app/reasons';
import isActiveContext from '../../app/isActiveContext';
import getTransactionMode from './getTransactionMode';
import Message from '../../components/Message';
import './TransactionList.css';

const dayFormatter = new Intl.DateTimeFormat(undefined, {
  month: 'long',
  day: 'numeric',
});

const weekdayFormatter = new Intl.DateTimeFormat(undefined, {
  weekday: 'long',
});

const timeFormatter = new Intl.DateTimeFormat(undefined, {
  hour: 'numeric',
  minute: 'numeric',
});

const dateFormatter = new Intl.DateTimeFormat(undefined, {
  year: 'numeric',
  month: 'long',
  day: '2-digit',
});

const oneDay = 24 * 60 * 60 * 1000;

function isToday(time) {
  const today = new Date();
  const todayStart = new Date(
    today.getFullYear(), today.getMonth(), today.getDate(), 0, 0, 0);
  const todayEnd = new Date(
    today.getFullYear(), today.getMonth(), today.getDate(), 23, 59, 59);
  return time >= todayStart.getTime() && time <= todayEnd.getTime();
}

function formatDay(dayDate) {
  const diff = Date.now() - dayDate.getTime();

  if (diff < oneDay) {
    return "Today";
  }
  if (diff < oneDay * 2) {
    return "Yesterday";
  }
  if (diff < oneDay * 7) {
    return weekdayFormatter.format(dayDate);
  }

  return dayFormatter.format(dayDate);
}

function formatBetween(start, end) {
  return dateFormatter.format(start) + " to " + formatDay(new Date(end));
};

function TransactionList() {
  const isActive = useContext(isActiveContext);
  const listRef = useRef();

  const [activeTransaction, setActiveTransaction] = useState(-1);
  const [start, setStart] = useState(Date.now() - oneDay * 7);
  const [end, setEnd] = useState(null);

  const getEnd = useCallback(() => end === null ? Date.now() : end, [end]);

  const filterBetween = transactions => {
    return transactions.reverse().filter(
      transaction => {
        return transaction.time >= start && transaction.time < getEnd();
      }
    );
  };

  const selectTransactions = createSelector(
    [state => filterBetween(state.transactions.slice())],
    transactions => {
      let byDay = {};
      let transaction;
      let day;
      let i = 0;
      for (; i < transactions.length; i++) {
        transaction = transactions[i];
        day = Math.trunc(transaction.time / 1000 / 60 / 60 / 24);
        if (day in byDay) {
          byDay[day].push(transaction);
        } else {
          byDay[day] = [transaction];
        }
      }
      return byDay;
    }
  );

  const data = useSelector(selectTransactions);
  const total = useSelector(state => state.transactions.length);

  const transactionCount = Object.values(data).reduce(
    (acc, val) => acc + val.length,
    0
  );

  const renderTransactions = () => {
    let lis = [];
    let index = 0;
    Object.entries(data).reverse().forEach(([ day, transactions ]) => {
      const dayDate = new Date(day * oneDay + 1);
      lis.push(
        <li className="header" key={day}>
          {formatDay(dayDate)}
        </li>
      );
      transactions.forEach((transaction) => {
        const timeDate = new Date(transaction.time);
        const mode = getTransactionMode(transaction);
        const selected = activeTransaction === index ? ' selected' : '';
        lis.push(
          <li className={`transaction${selected}`} key={transaction.time}>
            <img
              src={mode === MODES.SET
                ? "/reasons/0.svg"
                : `/reasons/${mode}/${transaction.reason}.svg`
              }
              alt={getReasonName(transaction.reason, mode)}
              height="16"
              width="16"
            />
            <time dateTime={timeDate.toISOString()}>
              {timeFormatter.format(timeDate)}
            </time>
            <span className={`amount ${mode}`}>{transaction.amount}</span>
          </li>
        );
        index++;
      });
    });
    return lis;
  }

  const handleKeyDown = useCallback(
    (event) => {
      if (!isActive) {
        return;
      }
      if (event.key === 'ArrowLeft') {
        event.preventDefault();
        setStart(start - oneDay * 7);
        setEnd(getEnd() - oneDay * 7);
        setActiveTransaction(-1);
      }
      if (event.key === 'ArrowRight') {
        event.preventDefault();
        const newEnd = getEnd() + oneDay * 7;
        if (isToday(newEnd) || newEnd >= Date.now()) {
          setStart(Date.now() - oneDay * 7);
          setEnd(null);
        } else {
          setStart(start + oneDay * 7);
          setEnd(newEnd);
        }
        setActiveTransaction(-1);
      }
      if (event.key === 'SoftRight' || event.key === 'j') {
        event.preventDefault();
        const nextTransaction = activeTransaction + 1 < transactionCount
          ? activeTransaction + 1
          : 0;
        if (nextTransaction !== activeTransaction) {
          setActiveTransaction(nextTransaction);
        }
      }
      if (event.key === 'SoftLeft' || event.key === 'k') {
        event.preventDefault();
        const prevTransaction = activeTransaction - 1 >= 0
          ? activeTransaction - 1
          : transactionCount - 1;
        if (prevTransaction !== activeTransaction) {
          setActiveTransaction(prevTransaction);
        }
      }
    },
    [
      isActive,
      start, setStart,
      getEnd, setEnd,
      transactionCount,
      activeTransaction, setActiveTransaction,
    ]
  );

  const scrollToTransaction = useCallback(
    (index) => {
      if (
        isActive &&
        listRef.current &&
        listRef.current.firstElementChild.nodeName === 'UL' &&
        index in listRef.current.firstElementChild.childNodes
      ) {
        listRef.current.scrollTop =
          listRef.current.firstElementChild.childNodes[index].offsetTop -
          listRef.current.offsetTop;
      }
    },
    [isActive, listRef]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', handleKeyDown);
      scrollToTransaction(activeTransaction);
      return () => document.removeEventListener('keydown', handleKeyDown);
    },
    [handleKeyDown, activeTransaction, scrollToTransaction]
  );

  const renderBetween = () => (
    <div className="between">
      <p>
        {formatBetween(start, getEnd())}
      </p>
    </div>
  );

  const renderErrorMessage = () => (
    <Message type="error">
      {total === 0
        ? (
          <>
            <h4>Zero Transactions</h4>
            <p>Your <strong>Guita</strong> wallet is empty.</p>
            <p>Start typing your initial balance to create a transaction.</p>
          </>
        ) : (
          <>
            <h4>Empty Period</h4>
            <p>
              There are no transactions from {formatBetween(start, getEnd())}.
            </p>
          </>
        )
      }
    </Message>
  );

  return (
    <div className="transacion-screen">
      <div className="transacion-list" ref={listRef}>
        {transactionCount > 0
          ? <ul className="transactions">{renderTransactions()}</ul>
          : renderErrorMessage()
        }
      </div>
      {renderBetween()}
    </div>
  );
}

export default TransactionList;
