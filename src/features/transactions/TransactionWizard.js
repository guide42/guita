import React, { useState, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ToastContext from '../../app/ToastContext';
import { MODES } from '../../app/modes';
import getTransactionMode from './getTransactionMode';
import { addTransaction } from './transactionsSlice';
import { incrementByAmount } from '../cash/cashSlice';
import { NEG_REASONS, POS_REASONS, SET, getReasonName } from '../../app/reasons';
import EnterAmount from '../../components/EnterAmount';
import ChoseReason from '../../components/ChoseReason';
import './TransactionWizard.css';

function TransactionWizard() {
  const dispatch = useDispatch();
  const cash = useSelector(state => state.cash.amount);
  const toast = useContext(ToastContext).toast;

  const [transaction, setTransaction] = useState({amount: 0.0, reason: null});
  const [step, setStep] = useState('amount');
  const [active, setActive] = useState(false);

  const show = () => {
    setActive(true);
  };
  const hide = () => {
    setActive(false);
  };

  const save = (payload) => {
    dispatch(addTransaction(payload));
    dispatch(incrementByAmount(payload.amount));
    const mode = getTransactionMode(payload);
    const reasonName = getReasonName(payload.reason, mode);
    toast(
      <>
        I just {payload.amount > 0 ? "got" : "took"}
          <span className={`amount ${mode}`}>{payload.amount}</span>
        {payload.amount > 0 ? "from" : "for"}
          <img src={`/reasons/${mode}/${payload.reason}.svg`} alt={reasonName} />
          <em>{reasonName}</em>
      </>
    );
  };

  const saveSet = (payload) => {
    dispatch(addTransaction(payload));
    dispatch(incrementByAmount(payload.amount - cash));
    toast(
      <>
        I have
          <span className="amount set">{payload.amount}</span>
        in
          <img src={`/reasons/0.svg`} alt="SET" />
          my wallet
      </>
    );
  };

  const setAmount = (amount) => {
    setTransaction({...transaction, amount});
  };
  const setReason = (reason) => {
    setTransaction({...transaction, reason});
  };

  const reset = () => {
    setTransaction({amount: 0.0, reason: null});
    setStep('amount');
    setActive(false);
  };

  const handleCompleteAmount = (amount, mode) => {
    if (mode === MODES.SET) {
      setReason(SET);
      saveSet({...transaction, time: Date.now(), amount, reason: SET});
      reset();
      return;
    }
    if (mode === MODES.NEG && amount > 0) {
      setAmount(amount * -1);
    } else {
      setAmount(amount);
    }
    setStep('reason');
  };

  const handleCancelReason = () => {
    setStep('amount');
  };

  const handleCompleteReason = (reason) => {
    setReason(reason);
    save({...transaction, time: Date.now(), reason});
    reset();
  };

  return (
    <div className="wizard" style={{display: active ? "block" : "none"}}>
      {step === 'amount' && (
        <EnterAmount
          initialMode={cash === 0 ? MODES.SET : MODES.NEG}
          initialAmount={transaction.amount}
          cash={cash}
          onStart={show}
          onEmpty={hide}
          onComplete={handleCompleteAmount}
        />
      )}
      {step === 'reason' && (
        <ChoseReason
          initialReason="9"
          reasons={transaction.amount > 0 ? POS_REASONS : NEG_REASONS}
          mode={transaction.amount > 0 ? MODES.POS : MODES.NEG}
          onCancel={handleCancelReason}
          onComplete={handleCompleteReason}
        />
      )}
    </div>
  );
}

export default TransactionWizard;
