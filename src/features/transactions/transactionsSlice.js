import { createSlice } from '@reduxjs/toolkit';

export const transactionsSlice = createSlice({
  name: 'transactions',
  initialState: [],
  reducers: {
    addTransaction(state, action) {
      const { time, amount, reason } = action.payload;
      state.push({ time, amount, reason });
    },
    removeTransaction(state, action) {
      const time = action.payload;
      return state.filter(
        (transaction) => transaction.time !== time
      );
    },
    removeAll() {
      return [];
    },
  },
});

export const {
  addTransaction,
  removeTransaction,
  removeAll,
} = transactionsSlice.actions;

export default transactionsSlice.reducer;
