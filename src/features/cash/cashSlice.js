import { createSlice } from '@reduxjs/toolkit';

export const cashSlice = createSlice({
  name: 'cash',
  initialState: {
    amount: 0.0,
  },
  reducers: {
    incrementByAmount(state, action) {
      state.amount += action.payload;
    },
    decrementByAmount(state, action) {
      state.amount -= action.payload;
    },
    setAmount(state, action) {
      state.amount = action.payload;
    },
    clear() {
      return {
        amount: 0.0,
      };
    },
  },
});

export const {
  incrementByAmount, decrementByAmount,
  setAmount, clear,
} = cashSlice.actions;

export default cashSlice.reducer;
