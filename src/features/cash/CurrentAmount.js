import React from 'react';
import Amount from '../../components/Amount';
import { useSelector } from 'react-redux';

function CurrentAmount() {
  const amount = useSelector(state => state.cash.amount);

  return (
    <Amount amount={amount} />
  );
}

export default CurrentAmount;
