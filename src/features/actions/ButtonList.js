import React, { useState, useCallback, useEffect, useRef, useContext } from 'react';
import isActiveContext from '../../app/isActiveContext';
import './ButtonList.css';

function ButtonList({ children }) {
  const listRef = useRef();
  const isActive = useContext(isActiveContext);
  const [activeButton, setActiveButton] = useState(-1);
  const childrenCount = React.Children.count(children);

  const handleKeyDown = useCallback(
    (event) => {
      if (!isActive) {
        return;
      }
      if (event.key === 'Enter' && activeButton > -1) {
        event.preventDefault();
        if (listRef.current && activeButton in listRef.current.childNodes) {
          listRef.current.childNodes[activeButton].click();
        }
      }
      if (event.key === 'SoftRight' || event.key === 'j') {
        event.preventDefault();
        const nextButton = activeButton + 1 < childrenCount
          ? activeButton + 1
          : -1;
        if (nextButton !== activeButton) {
          setActiveButton(nextButton);
        }
      }
      if (event.key === 'SoftLeft' || event.key === 'k') {
        event.preventDefault();
        const prevButton = activeButton - 1 >= -1
          ? activeButton - 1
          : childrenCount - 1;
        if (prevButton !== activeButton) {
          setActiveButton(prevButton);
        }
      }
    },
    [isActive, childrenCount, activeButton, setActiveButton]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', handleKeyDown);
      if (!isActive && activeButton !== -1) {
        setActiveButton(-1);
      }
      return () => document.removeEventListener('keydown', handleKeyDown);
    },
    [handleKeyDown, activeButton, setActiveButton, isActive]
  );

  const renderChildren = () => React.Children.map(children,
    (button, index) => React.cloneElement(button, {
      isSelected: index === activeButton,
    })
  );

  return (
    <div className="button-list" ref={listRef}>
      {renderChildren()}
    </div>
  );
}

export default ButtonList;
