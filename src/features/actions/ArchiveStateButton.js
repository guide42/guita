import React, { useContext } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { createSelector } from '@reduxjs/toolkit';
import { MODES } from '../../app/modes';
import ToastContext from '../../app/ToastContext';
import ConfirmActionButton from './ConfirmActionButton';
import getTransactionMode from '../transactions/getTransactionMode';
import { removeAll } from '../transactions/transactionsSlice';
import { clear } from '../cash/cashSlice';

const TIMEOUT = 3000;

function getStartOfYear(year) {
  return new Date(year, 0, 1, 0, 0, 0).getTime();
}

function getStartOfWeek(date) {
  let startOfWeek = new Date(date.getTime())
  startOfWeek.setDate(date.getDate() - date.getDay());
  startOfWeek.setHours(0, 0, 0);
  return startOfWeek.getTime();
}

const oneWeek = 7 * 24 * 60 * 60 * 1000;

function getWeekOfYear(date) {
  const startOfYear = getStartOfYear(date.getUTCFullYear());
  const startOfWeek = getStartOfWeek(date);
  return Math.round((startOfWeek - startOfYear) / oneWeek) + 1;
}

const selectTransactions = createSelector(
  [state => state.transactions.slice()],
  transactions => {
    let byWeek = {};
    let total = 0;
    for (let i = 0; i < transactions.length; i++) {
      const date = new Date(transactions[i].time);
      const week = date.getUTCFullYear() + '-' + getWeekOfYear(date);
      const mode = getTransactionMode(transactions[i]);
      if (week in byWeek) {
        if (mode === MODES.SET) {
          byWeek[week]['amount'] = transactions[i].amount;
        } else {
          byWeek[week]['amount'] += transactions[i].amount;
        }
        byWeek[week]['transactions'].unshift(transactions[i]);
      } else {
        byWeek[week] = {
          amount: total + transactions[i].amount,
          transactions: [transactions[i]],
        };
      }
      if (mode === MODES.SET) {
        total = transactions[i].amount;
      } else {
        total += transactions[i].amount;
      }
    }
    return byWeek;
  }
);

function ArchiveStateButton({ isSelected }) {
  const dispatch = useDispatch();
  const toast = useContext(ToastContext).toast;
  const data = useSelector(selectTransactions);

  const action = () => {
    const periods = Object.keys(data).length;

    if (periods === 0) {
      toast('My wallet is empty, yours?');
      return;
    }

    if (!navigator.getDeviceStorage) {
      toast('I have no storage devices.');
      return;
    }

    const storages = navigator.getDeviceStorages('sdcard');

    if (storages.length === 0) {
      toast('I have no SD card.');
      return;
    }

    const storage = storages.pop();
    const archive = (week, data) => {
      return new Promise((resolve, reject) => {
        const json = JSON.stringify(data);
        const file = new Blob([json], {type: 'application/json'});

        const creating = storage.addNamed(file, `guita/${week}.json`);

        const to = setTimeout(
          () => reject(
            new DOMError('TimeoutError', `Archive week ${week} timeout`)
          ),
          TIMEOUT
        );

        creating.onsuccess = () => {
          resolve('saved');
        };

        creating.onerror = function() {
          clearTimeout(to);
          if (this.error.name === 'NoModificationAllowedError') {
            resolve('skipped');
          } else {
            reject(this.error);
          }
        };
      });
    };

    Promise.all(
      Object.entries(data).map(([ week, data ]) => archive(week, data))
    )
    .catch(error => {
      toast(
        <>Ouch! I got pinched by <em>{error.name}</em> {error.message}.</>
      );
    })
    .then(results => {
      const archived = results.filter(value => value === 'saved').length;

      dispatch(removeAll());
      dispatch(clear());

      toast(
        <>
          My wallet is now empty.<br />
          I archived {archived} of {periods} week periods.
        </>
      );
    });
  };

  return (
    <ConfirmActionButton isSelected={isSelected} action={action}>
      Archive State
    </ConfirmActionButton>
  );
}

export default ArchiveStateButton;
