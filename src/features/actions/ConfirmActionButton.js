import React, { useState, useContext, useCallback, useEffect } from 'react';
import ToastContext from '../../app/ToastContext';
import isActiveContext from '../../app/isActiveContext';

function ConfirmActionButton({ isSelected, action, children }) {
  const [clearId, setClearId] = useState(null);
  const isActive = useContext(isActiveContext);
  const toast = useContext(ToastContext).toast;
  const handleOnClick = () => {
    if (clearId === null) {
      setClearId(setTimeout(
        () => {
          setClearId(null);
          toast(<>I had 5s to press <kbd>#</kbd> to <em>{children}</em></>);
        },
        5000
      ));
    }
  };

  const handleKeyDown = useCallback(
    (event) => {
      if (event.key === '#' && clearId !== null) {
        clearTimeout(clearId);
        setClearId(null);
        if (isActive) {
          event.preventDefault();
          action();
        }
      }
    },
    [clearId, setClearId, isActive, action]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', handleKeyDown);
      if (!isActive && clearId !== null) {
        clearTimeout(clearId);
        setClearId(null);
      }
      return () => document.removeEventListener('keydown', handleKeyDown);
    },
    [handleKeyDown, isActive, clearId, setClearId]
  );

  return (
    <button
      className={
        ((isSelected ? "selected" : "") + " " +
          (clearId === null ? "" : "disabled")).trim()}
      onClick={handleOnClick}
    >
      {children}
    </button>
  );
}

export default ConfirmActionButton;
