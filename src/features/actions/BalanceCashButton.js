import React, { useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createSelector } from '@reduxjs/toolkit';
import { MODES } from '../../app/modes';
import { SET } from '../../app/reasons';
import ToastContext from '../../app/ToastContext';
import ConfirmActionButton from './ConfirmActionButton';
import getTransactionMode from '../transactions/getTransactionMode';
import { addTransaction } from '../transactions/transactionsSlice';
import { setAmount } from '../cash/cashSlice';

const selectLastSet = createSelector(
  [(state) => state.transactions.slice().reverse()],
  (transactions) => {
    let lastSet = [];
    for (let i = 0; i < transactions.length; i++) {
      lastSet.push(transactions[i]);
      if (getTransactionMode(transactions[i]) === MODES.SET) {
        break;
      }
    }
    return lastSet.reverse();
  }
);

function BalanceCashButton({ isSelected }) {
  const dispatch = useDispatch();
  const toast = useContext(ToastContext).toast;
  const data = useSelector(selectLastSet);

  const action = () => {
    const newCash = data.reduce((acc, val) => acc + val.amount, 0);
    dispatch(setAmount(newCash));
    if (data.length > 1) {
      dispatch(addTransaction({
        time: Date.now(),
        amount: newCash,
        reason: SET,
      }));
    }
    toast(
      <>
        {data.length === 1 && <>Balance already done.{" "}</>}
        I now have
          <span className="amount set">{newCash}</span>
        in my wallet
      </>
    );
  };

  return (
    <ConfirmActionButton isSelected={isSelected} action={action}>
      Balance Cash
    </ConfirmActionButton>
  );
}

export default BalanceCashButton;
