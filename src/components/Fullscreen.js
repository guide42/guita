import React, { useState, useRef, useCallback, useEffect } from 'react';
import isActiveContext from '../app/isActiveContext';
import './Fullscreen.css';

function Fullscreen({ children }) {
  const [activeScreen, setActiveScreen] = useState(0);

  const ref = useRef();

  const scrollToScreen = useCallback(
    (screen) => {
      const percentage = screen * 100 * -1;
      setTimeout(
        () => ref.current.style.transform = `translate(0, ${percentage}vh)`,
        40
      );
    },
    [ref]
  );

  const childrenCount = React.Children.count(children);

  const handleKeyDown = useCallback(
    (event) => {
      if (event.key === 'ArrowUp') {
        event.preventDefault();
        const prevScreen = Math.max(activeScreen - 1, 0);
        if (prevScreen !== activeScreen) {
          setActiveScreen(prevScreen);
        }
      }
      if (event.key === 'ArrowDown') {
        event.preventDefault();
        const nextScreen = Math.min(activeScreen + 1, childrenCount - 1);
        if (nextScreen !== activeScreen) {
          setActiveScreen(nextScreen);
        }
      }
    },
    [childrenCount, activeScreen, setActiveScreen]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', handleKeyDown);
      scrollToScreen(activeScreen);
      return () => document.removeEventListener('keydown', handleKeyDown);
    },
    [handleKeyDown, scrollToScreen, activeScreen]
  );

  const renderChildren = () => React.Children.map(children,
    (child, index) => (
      <isActiveContext.Provider value={activeScreen === index}>
        {child}
      </isActiveContext.Provider>
    )
  );

  return (
    <div className="fullscreen" ref={ref}>{renderChildren()}</div>
  );
}

export default Fullscreen;
