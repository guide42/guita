import React from 'react';
import './Amount.css';

function formatAmount(amount) {
  return amount.toString();
}

function Amount({ amount }) {
  const formatedAmount = formatAmount(amount);
  const fontSize =
    formatedAmount.length > 3
      ? formatedAmount.length > 5
        ? formatedAmount.length > 7
          ? 40
          : 60
        : 80
      : 100;

  return (
    <mark className="amount">
      <span style={{fontSize: `${fontSize}%`}}>
        {formatedAmount}
      </span>
    </mark>
  );
}

export default Amount;
