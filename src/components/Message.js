import React from 'react';
import './Message.css';

export const TYPES = ['error', 'info'];

function Message({ children, type }) {
  if (!TYPES.includes(type)) {
    throw new Error(`Message type '${type}' not recognized`);
  }
  return (
    <div className={`message ${type}`}>
      {children}
    </div>
  );
}

export default Message;
