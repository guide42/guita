import React, { useContext } from 'react';
import ToastContext from '../app/ToastContext';
import './Toast.css';

function Toast() {
  const messages = useContext(ToastContext).messages;

  const renderMessages = () => messages.map(
    (msg, index) => <li key={msg + index}>{msg}</li>
  );

  return (
    <ul className="toast">{renderMessages()}</ul>
  );
}

export default Toast;
