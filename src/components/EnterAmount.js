import React, { useState, useCallback, useEffect } from 'react';
import { MODES } from '../app/modes';
import Amount from './Amount';
import './EnterAmount.css';

const KEYS = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', '*'];

function isAmountValid(amount) {
  const dotCount = Array.from(amount).reduce(
    (acc, val) => acc + (val === '.' ? 1 : 0),
    0
  );
  if (dotCount > 1) {
    return false;
  }
  return !isNaN(parseFloat(amount));
}

function EnterAmount({
  initialMode, initialAmount, cash,
  onStart, onEmpty, onComplete,
}) {
  const [mode, setMode] = useState(initialMode);
  const [amount, setAmount] = useState(
    initialAmount === 0 ? '' : Math.abs(initialAmount).toString()
  );

  const calcRealAmount = () => {
    const floatAmount = parseFloat(amount);
    if (isNaN(floatAmount)) {
      return '-' + cash.toString();
    }
    const realAmount = floatAmount - cash;
    const realSign = realAmount > 0 ? '+' : '';
    return realSign + realAmount.toString();
  };

  const handleKeyDown = useCallback(
    (event) => {
      if (KEYS.includes(event.key)) {
        let nextAmount;
        if (event.key === '*' || event.key === '.') {
          if (amount.length === 0) {
            nextAmount = '0.';
          } else {
            nextAmount = ''.concat(amount, '.');
          }
        } else {
          nextAmount = ''.concat(amount, event.key);
        }
        if (!isAmountValid(nextAmount)) {
          return;
        }
        if (amount.length === 0) {
          onStart();
        }
        setAmount(nextAmount);
      }
      if (event.key === 'Backspace') {
        if (amount.length === 0) {
          return;
        }
        event.preventDefault();
        if (amount.length === 1) {
          setAmount('');
          onEmpty();
        } else {
          setAmount(amount.slice(0, -1));
        }
      }
      if (event.key === 'Enter') {
        if (amount.length === 0) {
          return;
        }
        event.preventDefault();
        setMode(MODES.NEG);
        setAmount('');
        if (isAmountValid(amount)) {
          onComplete(parseFloat(amount), mode);
        }
      }
      if (event.key === 'Call' || event.key === 'c') {
        if (mode === MODES.NEG) {
          setMode(MODES.POS);
        }
        if (mode === MODES.POS) {
          setMode(MODES.SET);
        }
        if (mode === MODES.SET) {
          setMode(MODES.NEG);
        }
      }
    },
    [amount, setAmount, mode, setMode, onStart, onEmpty, onComplete]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', handleKeyDown);
      return () => document.removeEventListener('keydown', handleKeyDown);
    },
    [handleKeyDown]
  );

  if (!amount) {
    return null;
  }

  return (
    <div className={`enter ${mode}`}>
      <Amount amount={amount} />
      {mode === MODES.SET && (
        <span className="real">{calcRealAmount()}</span>
      )}
    </div>
  );
}

export default EnterAmount;
