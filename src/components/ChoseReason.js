import React, { useState, useCallback, useEffect } from 'react';
import './ChoseReason.css';

function ChoseReason({ initialReason, reasons, mode, onCancel, onComplete }) {
  const [reason, setReason] = useState(initialReason);

  const handleKeyDown = useCallback(
    (event) => {
      if (event.key in reasons) {
        setReason(event.key);
      }
      if (event.key === 'Backspace') {
        event.preventDefault();
        onCancel();
      }
      if (event.key === 'Enter') {
        event.preventDefault();
        onComplete(reason);
      }
    },
    [reasons, reason, setReason, onCancel, onComplete]
  );

  useEffect(
    () => {
      document.addEventListener('keydown', handleKeyDown);
      return () => document.removeEventListener('keydown', handleKeyDown);
    },
    [handleKeyDown]
  );

  const renderReasons = () => Object.entries(reasons).map(
    ([ key, description ]) => (
      <li key={description} className={key === reason ? "selected" : ""}>
        <label>{key}</label>
        <img src={`/reasons/${mode}/${key}.svg`} alt={description} />
        <small>{description}</small>
      </li>
    )
  );

  return (
    <div className="reasons">
      <ol>{renderReasons()}</ol>
    </div>
  );
}

export default ChoseReason;
