import React from 'react';
import './Loading.css';

function Loading() {
  return (
    <div className="loading">
      <img src="/icons/icon128x128.png" alt="Guita" />
    </div>
  );
}

export default Loading;
