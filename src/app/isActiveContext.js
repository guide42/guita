import React from 'react';

const isActiveContext = React.createContext(false);

export default isActiveContext;
