const NEG = 'neg';
const POS = 'pos';
const SET = 'set';

export const MODES = {NEG, POS, SET};
