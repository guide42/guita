import { MODES } from './modes';

export const NEG_REASONS = {
  1: "Food",
  2: "Shelter",
  3: "Transport",
  4: "Health",
  5: "Entertainment",
  6: "Communication",
  7: "Equipment",
  8: "Deposit",
  9: "Other",
};

export const POS_REASONS = {
  1: "Balance",
  2: "Withdraw",
  3: "Investment",
  4: "Lend",
  5: "Sell",
  6: "Found",
  7: "Changed",
  8: "Donation",
  9: "Other",
};

export const SET = 0;

export function getReasonName(reason, mode) {
  if (mode === MODES.SET || reason === 0) {
    return "";
  }
  if (mode === MODES.POS) {
    return POS_REASONS[reason];
  }
  return NEG_REASONS[reason];
}
