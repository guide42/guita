import React from 'react';

const ToastContext = React.createContext([]);

class ToastContextProvider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      toast: this.toast.bind(this),
    };
  }
  toast(message) {
    this.setState((prevState) => {
      const messages = [...prevState.messages, message];
      return {messages, toast: prevState.toast};
    });
    setTimeout(
      () => this.setState((prevState) => {
        const messages = prevState.messages.filter((msg) => msg !== message);
        return {messages, toast: prevState.toast};
      }),
      3000
    );
  }
  render() {
    return (
      <ToastContext.Provider value={this.state}>
        {this.props.children}
      </ToastContext.Provider>
    );
  }
}

export default ToastContext;
export { ToastContextProvider };
