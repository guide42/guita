import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import { persistStore, persistReducer,
  FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER } from 'redux-persist';
import localForage from 'localforage';
import cashReducer from '../features/cash/cashSlice';
import transactionsReducer from '../features/transactions/transactionsSlice';

localForage.config({
  name: 'guita',
  description: 'Transactions and current cash amount',
  version: 0.1,
});

const persistConfig = {
  key: 'guita',
  storage: localForage,
};

const rootReducer = combineReducers({
  cash: cashReducer,
  transactions: transactionsReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
});

const persistor = persistStore(store);

export { store, persistor };
