import React from 'react';
import { ToastContextProvider } from './app/ToastContext';
import Toast from './components/Toast';
import Fullscreen from './components/Fullscreen';
import CurrentAmount from './features/cash/CurrentAmount';
import TransactionList from './features/transactions/TransactionList';
import TransactionWizard from './features/transactions/TransactionWizard';
import ButtonList from './features/actions/ButtonList';
import BalanceCashButton from './features/actions/BalanceCashButton';
import ArchiveStateButton from './features/actions/ArchiveStateButton';
import './App.css';

function App() {
  return (
    <ToastContextProvider>
      <Toast />
      <Fullscreen>
        <header>
          <CurrentAmount />
        </header>
        <main>
          <TransactionList />
        </main>
        <footer>
          <div className="guita">
            <p><strong>Guita</strong> is <em>lunfardo</em> for cash.</p>
            <p><strong>Guita</strong> is the amount of money in your wallet, wherever you are.</p>
            <p><strong>Guita</strong> has no currency.</p>
          </div>
          <ButtonList>
            <BalanceCashButton />
            <ArchiveStateButton />
          </ButtonList>
          <dl>
            <dt>Credits</dt>
            <dd>
              <label>Dev</label>
              {" "}Juan M{" "}
              &laquo;<small style={{color: 'blue'}}>jm.guide42.com</small>&raquo;
            </dd>
            <dd>
              Thanks to Banana Hackers
              <br />
              Icons by Filip Zavadil
            </dd>
          </dl>
        </footer>
      </Fullscreen>
      <TransactionWizard />
    </ToastContextProvider>
  );
}

export default App;
