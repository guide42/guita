# Guita for KaiOS

Simple cash flow application for KaiOS/GerdaOS.

<img src="docs/home.png" />

<img src="docs/set.png" /> <img src="docs/neg.png" /> <img src="docs/neg-reasons.png" /> <img src="docs/pos.png" /> <img src="docs/pos-reasons.png" /> <img src="docs/transactions.png" />

# Usage

You current amount of money in your wallet is shown in big letters.

The first time, type your initial balance and confirm.

Quickly add new transactions on the go: start typing the amount spent (`*` is dot), confirm and chose the reason category.

Call button will change the input mode between: negative, positive and set. The latter will reset the current amount.

Press down arrow to see the transactions, left and right to change display period.

Use soft left and right to navigate through transactions of the selected period.

Press down again to see about and actions, use soft keys to navigate buttons, to execute press Enter and then `#`.

Balance Cash action will create a set transaction with your current amount.

Archive State will dump all transactions by week into the SD card before start a fresh.

## Reasons

| Neg | ati | ve | - | Pos | iti | ve |
| --- | --- | --- | --- | --- | --- | --- |
| <img src="public/reasons/neg/1.svg" width=16 height=16 /> | <img src="public/reasons/neg/2.svg" width=16 height=16 /> | <img src="public/reasons/neg/3.svg" width=16 height=16 /> | | <img src="public/reasons/pos/1.svg" width=16 height=16 /> | <img src="public/reasons/pos/2.svg" width=16 height=16 /> | <img src="public/reasons/pos/3.svg" width=16 height=16 /> |
| <img src="public/reasons/neg/4.svg" width=16 height=16 /> | <img src="public/reasons/neg/5.svg" width=16 height=16 /> | <img src="public/reasons/neg/6.svg" width=16 height=16 /> | | <img src="public/reasons/pos/4.svg" width=16 height=16 /> | <img src="public/reasons/pos/5.svg" width=16 height=16 /> | <img src="public/reasons/pos/6.svg" width=16 height=16 /> |
| <img src="public/reasons/neg/7.svg" width=16 height=16 /> | <img src="public/reasons/neg/8.svg" width=16 height=16 /> | <img src="public/reasons/neg/9.svg" width=16 height=16 /> | | <img src="public/reasons/pos/7.svg" width=16 height=16 /> | <img src="public/reasons/pos/8.svg" width=16 height=16 /> | <img src="public/reasons/pos/9.svg" width=16 height=16 /> |

## Thanks

Banana Hackers, Filip Zavadil.
