### Latest Version

### 0.3.0 (2020-10-21)

 - Balance Cash button will add a SET transaction.
 - Archive button to dump into SD card all transactions by week before clearing
   the state. Replaces clear state button.

### 0.2.0 (2020-09-11)

 - Use `*` as decimal punctuation.
 - Move through transactions of the selected period.
 - Talk to myself.
 - Buttons to re-calculate cash and clear state.

### 0.1.0 (2020-09-03)

First public version.
